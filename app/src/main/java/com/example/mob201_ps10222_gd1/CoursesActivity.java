package com.example.mob201_ps10222_gd1;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mob201_ps10222_gd1.ActivityCon.LopActivity;
import com.example.mob201_ps10222_gd1.ActivityCon.ThemSinhVien;

public class CoursesActivity extends AppCompatActivity {
    SQLiteDatabase database;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);
        statusBarSetting();
        createDatabase();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void statusBarSetting() {
        // Ẩn thanh status bar:
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void createDatabase() {
        database = openOrCreateDatabase("htht.db", MODE_PRIVATE, null);
        Log.d("db", "Tạo dữ liệu thành công");
        createTable();
    }
    public void createTable() {
        String sqlClass = "create table if not exists Lớp(stt integer primary key, MãLớp text, TênLớp text)";
        String sqlStudent = "create table if not exists SinhViên(stt integer primary key, MãSinhViên text, TênSinhViên text, TênLớp text)";
        database.execSQL(sqlClass);
        database.execSQL(sqlStudent);
    }
    public void reasetDatabase(View v) {
        if (deleteDatabase("htht.db")) {
            Log.d("db", "Xóa dữ liệu thành công");
            createDatabase();
        } else {
            Log.d("db", "Xóa dữ liệu không thành công");
        }
    }
    public void quanLyLop(View v  ) {
        Intent intent = new Intent(this, LopActivity.class);
        startActivity(intent);
    }

    public void quanLyHocVien(View v) {
        Intent intent = new Intent(this, ThemSinhVien.class);
        startActivity(intent);
    }

    public void troVe(View view) {
        finish();
    }
}

