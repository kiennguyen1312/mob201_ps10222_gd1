package com.example.mob201_ps10222_gd1.Fragment;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mob201_ps10222_gd1.R;

public class ThemLop extends Fragment {
    EditText txtClassID, txtClassName;
    Button btnClear, btnCreateClass;
    ImageView imageViewBack;
    SQLiteDatabase database;

    public static Fragment newInstance(Context context) {
        ThemLop fragment = new ThemLop();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_them_lop, null);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

        btnCreateClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = getActivity().openOrCreateDatabase("htht.db", Context.MODE_PRIVATE, null);
                ContentValues values = new ContentValues();
                values.put("MãLớp", txtClassID.getText().toString());
                values.put("TênLớp", txtClassName.getText().toString());
                String msg = "";
                if (database.insert("Lớp", null, values) != -1) {
                    msg = "Thêm thành công";
                } else {
                    msg = "Thêm không thành công";
                }
                Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtClassID.setText("");
                txtClassName.setText("");
                txtClassID.requestFocus();
            }
        });

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

    }

    public void init() {
        txtClassID = (EditText) getActivity().findViewById(R.id.txtClassID);
        txtClassName = (EditText) getActivity().findViewById(R.id.txtClassName);
        btnClear = (Button) getActivity().findViewById(R.id.btnClear);
        btnCreateClass = (Button) getActivity().findViewById(R.id.btnCreateClass);
        imageViewBack = (ImageView) getActivity().findViewById(R.id.imageview_back);
    }
}