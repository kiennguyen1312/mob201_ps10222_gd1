package com.example.mob201_ps10222_gd1.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.mob201_ps10222_gd1.R;
import java.util.ArrayList;

public class DanhSachLop extends Fragment {
    ListView ltvListClass;
    SQLiteDatabase database;

    ArrayList<String> list = null;
    ArrayAdapter<String> adapter = null;

    public static Fragment newInstance(Context context) {
        DanhSachLop fragment = new DanhSachLop();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ltvListClass = (ListView) getActivity().findViewById(R.id.ltvListClass);
        array();
        loadListClass();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_danh_sach_lop, null);
        return root;

    }

    public void array() {
        list = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, list);
        ltvListClass.setAdapter(adapter);
        ltvListClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), list.get(i), Toast.LENGTH_SHORT).show();
                final String data = list.get(i);
                final int pos = i;
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setTitle("Xóa");
                b.setMessage("Bạn có muốn xóa không " + data.toString() + " Không ?");
                b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        database.delete("Lớp", "stt=?",
                                new String[] { data.toString() });

                        Toast.makeText(getContext(), "Xóa thành công",
                                Toast.LENGTH_LONG).show();
                        list.remove(pos);
                        adapter.notifyDataSetChanged();
                    }
                });
                b.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                b.show();
            }
        });
    }

    // Load Data
    public void loadListClass() {
        database = getActivity().openOrCreateDatabase("htht.db", Context.MODE_PRIVATE, null);
        Cursor c = database.query("Lớp", null, null, null, null, null,
                null);
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            list.add("  " + c.getString(0) + "    -    "
                    + c.getString(1) + "    -    " + c.getString(2));
            c.moveToNext();
        }
        c.close();
        adapter.notifyDataSetChanged();
    }
}
