package com.example.mob201_ps10222_gd1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

public class XemTin extends Activity {
    WebView webView;
    String link;
    ProgressDialog progressBar;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_xemtin);
        statusBarSetting();
        AnhXa();
        link = getIntent().getExtras().getString("link");
        webView.loadUrl(link);
        // Cài đặt cho trang web
        setting();

        progressBar = ProgressDialog.show(this, "System", "Loading...");
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (progressBar.isShowing())
                    progressBar.dismiss();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void statusBarSetting() {
        // Ẩn thanh status bar:
        // Đổi màu StatusBar:
        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.purple));
    }

    private void setting() {
        // Phóng to thu nhỏ màn hình
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);

        // Ẩn icon phóng to thu nhỏ
        webView.getSettings().setDisplayZoomControls(false);

//        // Căn chỉnh kích thước web (Chỉ dùng khi web không có responsive)
//        webView.setPadding(0, 0, 0, 0);
//        webView.setInitialScale(getScale());
    }

    // Lấy kích thước màn hình đt để set kích thước trang web
    private int getScale(){
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width)/new Double(width);
        val = val * 100d;
        return val.intValue();
    }

    private void AnhXa() {
        webView = (WebView) findViewById(R.id.webView);
    }

    public void clickBack(View view) {
        finish();
    }
}
