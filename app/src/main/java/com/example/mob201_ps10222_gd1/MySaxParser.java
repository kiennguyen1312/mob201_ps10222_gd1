package com.example.mob201_ps10222_gd1;

import android.util.Log;
import com.example.mob201_ps10222_gd1.Model.Item;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;

public class MySaxParser {
    public static ArrayList<Item> xmlParser(InputStream is){
        ArrayList<Item> items = null;
        try {
            // Tạo xmlReader từ xmlParser
            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            // Tạo saxhandler
            MySaxHandler saxHandler = new MySaxHandler();
            // Lưu handler vào xmlreader
            xmlReader.setContentHandler(saxHandler);
            xmlReader.parse(new InputSource(is));
            items = saxHandler.getItems();
        } catch (Exception e){
            Log.d("Lỗi", "Lấy không được " + e.toString());
        } return items;
    }
}