package com.example.mob201_ps10222_gd1;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.example.mob201_ps10222_gd1.Model.Item;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class NewsActivity extends AppCompatActivity {

    ArrayList<Item> items = new ArrayList<Item>();
    String diachi_rss = "https://vietnamnet.vn/rss/giao-duc.rss";
    ListView lv_tintuc;
    Toolbar myToolbar;

//    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        statusBarSetting();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        MyAsyncTask gandulieu = new MyAsyncTask();
        gandulieu.execute();

        lv_tintuc = (ListView) findViewById(R.id.lv_tintuc);
        lv_tintuc.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3){
                // "arg2" is position
                String link = items.get(arg2).getLink();
                Intent intent = new Intent(getApplicationContext(),XemTin.class);
                intent.putExtra("link", link);
                startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void statusBarSetting() {
        // Ẩn thanh status bar:
        // Đổi màu StatusBar:
        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.purple));
    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(NewsActivity.this, "System", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(diachi_rss);
                URLConnection connection = url.openConnection();
                InputStream is = connection.getInputStream();
                items = (ArrayList<Item>) MySaxParser.xmlParser(is);

            } catch (Exception e){
                e.printStackTrace();
            } return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            dialog.dismiss();
            try{
                MyAdapter adapter = new MyAdapter(getApplicationContext(), R.layout.custom_tintheoloai_item, items);
                lv_tintuc.setAdapter(adapter);
            } catch (Exception e){
                Log.d("title","Adapter không được");
            }
        }
    }

    class MyAdapter extends ArrayAdapter<Item> {
        Context context;
        ArrayList<Item> items;

        // Constructor:
        public MyAdapter (Context context, int textViewResourceId, ArrayList<Item>objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.items = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowview = inflater.inflate(R.layout.custom_tintheoloai_item, parent, false);

            TextView tv_title = (TextView) rowview.findViewById(R.id.title);
            TextView tv_description = (TextView) rowview.findViewById(R.id.description);
            TextView tv_pubdate = (TextView) rowview.findViewById(R.id.pubdate);

            tv_title.setText(items.get(position).getTittle().toString());
            tv_description.setText(items.get(position).getDescription().toString());
            tv_pubdate.setText(items.get(position).getPubdate().toString());

            return  rowview;
        }
    }

    public void clickBack(View view) {
        finish();
    }

}
