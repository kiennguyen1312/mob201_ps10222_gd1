package com.example.mob201_ps10222_gd1.ActivityCon;

import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.mob201_ps10222_gd1.Adapter.PageAdapter;
import com.example.mob201_ps10222_gd1.R;

public class LopActivity extends FragmentActivity {
    private ViewPager _mViewPager;
    private PageAdapter _adapter;
    private Button _btn1,_btn2;


    /** Called when the activity is first created. */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lop);

        statusBarSetting();
        setUpView();
        setTab();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void statusBarSetting() {
        // Ẩn thanh status bar:
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void setUpView(){
        _mViewPager = (ViewPager) findViewById(R.id.viewPager);
        _adapter = new PageAdapter(getApplicationContext(),getSupportFragmentManager());
        _mViewPager.setAdapter(_adapter);
        _mViewPager.setCurrentItem(0);
        initButton();
    }
    private void setTab(){
        _mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrollStateChanged(int position) {}
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                btnAction(position);
            }
        });
    }

    private void btnAction(int action){
        switch(action){
            case 0:
                setButton(_btn1,null,40,40); setButton(_btn2,"",20,20);
                break;
            case 1:
                setButton(_btn2,null,40,40); setButton(_btn1,"",20,20);
                break;
        }
    }
    private void initButton(){
        _btn1=(Button)findViewById(R.id.btn1);
        _btn2=(Button)findViewById(R.id.btn2);;
        setButton(_btn1,"",40,40);
        setButton(_btn2,"",20,20);;
    }
    private void setButton(Button btn,String text,int h, int w){
        btn.setWidth(w);
        btn.setHeight(h);
        btn.setText(text);
    }

}