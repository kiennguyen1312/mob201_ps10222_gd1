package com.example.mob201_ps10222_gd1.Adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.mob201_ps10222_gd1.Fragment.DanhSachLop;
import com.example.mob201_ps10222_gd1.Fragment.ThemLop;

public class PageAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage=2;

    public PageAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context=context;

    }
    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch(position){
            case 0:
                f = ThemLop.newInstance(_context);
                break;
            case 1:
                f = DanhSachLop.newInstance(_context);
                break;
        }
        return f;
    }
    @Override
    public int getCount() {
        return totalPage;
    }

}